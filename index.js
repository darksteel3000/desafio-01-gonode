const express = require('express');
const nunjucks = require('nunjucks');
const path = require('path');
const bodyParser = require('body-parser');
const moment = require('moment');

const app = express();

nunjucks.configure('views', {
  autoescape: true,
  express: app,
});

app.set('view engine', 'njk');
app.set('views', path.join(__dirname, 'views'));

app.use(bodyParser.urlencoded({ extended: false }));

function calculaIdade(dataNascimento) {
  const parse = moment(dataNascimento, 'DD/MM/YYYY');
  if (parse.isValid()) {
    return moment().diff(parse, 'years');
  }
  return null;
}

const checarParametrosMiddleware = (req, res, next) => {
  if (!req.params.nome || !req.params.idade) {
    res.redirect('/');
  } else {
    next();
  }
};

app.get('/', (req, res) => {
  res.render('main');
});

app.post('/check', (req, res) => {
  const idade = calculaIdade(req.body.dataNascimento);

  if (idade && req.body.nome) {
    let prefixoUrl = '/major';
    if (idade < 18) {
      prefixoUrl = '/minor';
    }
    res.redirect(`${prefixoUrl}/${req.body.nome}/${idade}`);
  } else {
    res.status(500).send('Dados inválidos.');
  }
});

app.get('/major/:nome/:idade', checarParametrosMiddleware, (req, res) => {
  res.render('major', { nome: req.params.nome });
});

app.get('/minor/:nome/:idade', checarParametrosMiddleware, (req, res) => {
  res.render('minor', { nome: req.params.nome });
});

app.listen(3000);
